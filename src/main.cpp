// Program that creates an auto structure for c++ in the current folder.
// All files and folders are created from the current folder.

#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

// Checks if either files or folders already exists, returns true if exists
int checkExists(std::string path) {
  if (std::filesystem::exists(path)) {
    std::cout << path << " already exists. \n";
    return 1;
  }
  return 0;
}

// Creates the main source file
int createSRC(std::string prj) {
  std::fstream file{};
  std::string filename{"src/main.cpp"};
  if (checkExists(filename)) {
    return 1;
  }
  file.open(filename, std::ios::out);
  if (!file) {
    std::cout << "Failed to create main.cpp! \n";
    return 1;
  }
  file << "#include <iostream>" << std::endl;
  file << "#define PROJECT_NAME \"" << prj << "\"" << std::endl;
  file << "" << std::endl;
  file << "int main(int argc, char **argv) {" << std::endl;
  file << "  if (argc != 1) {" << std::endl;
  file << "    std::cout << argv[0] << \"takes no arguments.\\n\";"
       << std::endl;
  file << "    return 1;" << std::endl;
  file << "  }" << std::endl;
  file << "  std::cout << \"This is project \" << PROJECT_NAME << \".\\n\";"
       << std::endl;
  file << "  return 0;" << std::endl;
  file << "}" << std::endl;
  file.close();
  std::cout << "Created main.cpp \n";
  return 0;
}

// Creates meson.build file
int createMeson(std::string prj) {
  std::filesystem::path pwd = std::filesystem::current_path();
  std::ofstream file{};
  std::string filename{"meson.build"};
  if (checkExists(filename)) {
    return 1;
  }
  file.open(filename, std::ios::out);
  if (!file) {
    std::cout << "Failed to create meson.build! \n";
    return 1;
  }
  file << "project('" << prj
       << "', 'cpp', default_options : ['cpp_std=c++17', 'prefix="
       << pwd.string() << "'])" << std::endl;
  file << "executable('" << prj << "', './src/main.cpp', install : true)"
       << std::endl;
  file.close();
  std::cout << "Created meson.build \n";
  return 0;
}

// Creates .projectile file
int createProj() {
  std::ofstream file{};
  std::string filename{".projectile"};
  if (checkExists(filename)) {
    return 1;
  }
  file.open(filename, std::ios::out);
  if (!file) {
    std::cout << "Failed to create .projectile! \n";
    return 1;
  }
  file.close();
  std::cout << "Created .projectile \n";
  return 0;
}

// Creates folders listed in the vector
int createFolders() {
  std::vector<std::string> folders{"build", "bin", "lib", "include", "src"};
  int fails{};
  for (int i = 0; i < static_cast<int>(folders.size()); i++) {
    if (!checkExists(folders.at(i))) {
      std::filesystem::create_directory(folders.at(i));
    } else {
      fails++;
    }
  }
  if (static_cast<int>(folders.size()) == fails) {
    return 1;
  }
  return 0;
}

// Main function
int main(int argc, char **argv) {

  // Making sure a project name is set
  std::string prjname{};
  if (argv[1] == NULL) {
    std::cout << "Enter a project name: " << '\n';
    std::getline(std::cin >> std::ws, prjname);
  } else
    prjname = argv[1];

  // Create local repository with git
  FILE *gitpipe{popen("git init", "w")};
  pclose(gitpipe);

  // Create folders
  createFolders();

  // Create main source file
  createSRC(prjname);

  // Create .projectile file, needed for projectile in emacs if git isn't used
  // createProj();

  // Creating meson.build file, needed for building and creating links for
  // program
  createMeson(prjname);

  // Initializing meson
  FILE *pipe{popen("meson setup ./build", "w")};
  pclose(pipe);

  // Building project
  FILE *newpipe{popen("meson install -C ./build", "w")};
  pclose(newpipe);

  // Copying compile_commands.json, for use with language servers for auto
  // completion
  if (std::filesystem::exists("./build/compile_commands.json")) {
    std::filesystem::rename("./build/compile_commands.json",
                            "compile_commands.json");
  } else
    std::cout << "compile_commands.json doesn't exist at location. \n";

  return 0;
}
